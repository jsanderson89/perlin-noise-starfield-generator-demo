package  
{
	/**
	 * ...
	 * @author a
	 */
	public class Block 
	{
		public var color:uint;
		public var xVar:int;
		public var yVar:int;
		public function Block(_color:uint, _x:int, _y:int) 
		{
			color = _color;
			xVar = _x;
			yVar = _y;
		}
	}
}