package  
{
	/**
	 * ...
	 * @author a
	 */
	public class PixelColorValues 
	{
		public var red:Number;
		public var blue:Number;
		public var green:Number;
		
		public function PixelColorValues(_red:Number, _blue:Number, _green:Number) 
		{
			red = _red;
			blue = _blue;
			green = _green;
		}
		
	}

}