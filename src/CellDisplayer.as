package  
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author a
	 */
	public class CellDisplayer extends Sprite 
	{
		
		public function CellDisplayer(cellArray:Array) 
		{
			//trace("entered");
			//trace(cellArray.length);
			for (var i:int = 0; i <= cellArray.length-1; i++)
			{
				var cell:Cell = cellArray[i];
				addChild(cell);
				cell.x = cell.xVal;
				cell.y = cell.yVal;
				//trace(cell.xVal + ", " + cell.yVal)

				if(cell.checkIfBlank() == false)
				{
					removeChild(cell);
					cell = null;
					//trace("isn't drawable");
				}
			}
			
		}
		
	}

}