package  
{
	import BaseClasses.Utilities.Keycodes;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	/**
	 * ...
	 * @author a
	 */
	public class PerlinGenerator extends Sprite
	{
		private var myBitmapDataObject:BitmapData;
		private var bitmap:BitmapProcessorController;
		private var myBitmap:Bitmap;
		private var gameStage:Stage;
		
		public var blockSize:int = 20; 
		public var cellArray:Array;
		public var display:CellDisplayer;
		
		public function PerlinGenerator(mainStage:Stage) 
		{
			var i:Number = 0xFFFFFF - 9079434;
			var j:Number = 0x000000 - 9079434;
			//trace(i);
			//trace(j);
			
			gameStage = mainStage;
			gameStage.addEventListener(KeyboardEvent.KEY_UP, handleKeyboardEvent);
			this.addEventListener(MouseEvent.CLICK, handleMouseClick);
		}
		
		private function handleKeyboardEvent(e:KeyboardEvent):void
		{
			if (e.keyCode == Keycodes.DOWN_KEY)
			{
				if (bitmap)
				{
					if (bitmap.visible == true)
					{
						bitmap.visible = false;
					}
					
					else if (bitmap.visible == false)
					{
						bitmap.visible = true;
					}
				}
				else
				{
					trace("no bitmap");
				}
			}
			
			if (e.keyCode == Keycodes.UP_KEY)
			{
				if (myBitmap)
				{
					if (myBitmap.visible == true)
					{
						myBitmap.visible = false;
					}
					
					else if (myBitmap.visible == false)
					{
						myBitmap.visible = true;
					}
				}
				else
				{
					trace("no noise");
				}
			}
			
			if (e.keyCode == Keycodes.ENTER_KEY)
			{
				CreateBitmapHandler();
			}
			
			if (e.keyCode == Keycodes.SPACE_KEY)
			{
				if (bitmap != null)
				{
					removeChild(bitmap);
					bitmap = null;
					cellArray = null;
					if (display)
					{
						removeChild(display);
						display = null;
					}
				}
				
				if (myBitmapDataObject == null && myBitmap == null)
				{
					createBitmap();
				}
				
				else if (myBitmap != null || myBitmapDataObject != null)
				{
					if (myBitmapDataObject != null)
					{
						myBitmapDataObject = null;
					}
				
					if (myBitmap != null)
					{
						removeChild(myBitmap);
						myBitmap = null;
					}
					
				}
			}
			
			if (e.keyCode == Keycodes.BACKSPACE_KEY)
			{
				if (cellArray != null)
				{
					display = new CellDisplayer(cellArray);
					addChild(display);
				}
			}
		}
		
		private function createBitmap():void
		{
			myBitmapDataObject = new BitmapData(1024, 768);
			var seed:Number = Math.floor(Math.random()*100);
			myBitmapDataObject.perlinNoise(200, 200, 1, seed, true, true, 7, true, null);
			myBitmap = new Bitmap(myBitmapDataObject);
			addChild(myBitmap);
		}
		
		private function handleMouseClick(e:MouseEvent):void
		{
			CreateBitmapHandler();
		}
		
		private function CreateBitmapHandler():void
		{
			if (bitmap != null)
			{
				removeChild(bitmap);
				bitmap = null;
				cellArray = null;
				if (display)
				{
					removeChild(display);
					display = null;
				}
			}
			
			else
			{
				bitmap = new BitmapProcessorController(myBitmapDataObject, blockSize);
				addChild(bitmap);
				cellArray = bitmap.cellArray;
				//trace(cellArray);
			}
		}
	}
}