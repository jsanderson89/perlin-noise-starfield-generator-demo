package  
{
	import BaseClasses.Utilities.Colors;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author a
	 */
	public class Cell extends Sprite
	{
		public var xVal:int;
		public var yVal:int;
		public var blockWidth:int;
		public var color:uint;
		public var buffer:int = -200000;
		
		private var distanceFromWhite:uint;
		
		public function Cell(_xVal:int, _yVal:int, _blockWidt:int, _color:uint) 
		{
			xVal = _xVal;
			yVal = _yVal;
			blockWidth = _blockWidt;
			color = _color;
			distanceFromWhite = Colors.WHITE - color + buffer;
			
			draw();
		}
		
		public function draw():void
		{
				var position:Number = blockWidth / 2
				var size:Number = Math.random() * blockWidth / 2;
				var xAdjust:Number = position * Math.random();
				var yAdjust:Number = position * Math.random();
				
				var color:uint = getColor(size);
			
				this.graphics.beginFill(color);
				//trace("xval " + (xVal));
				//trace(yVal);
				
				
				this.graphics.drawCircle(xAdjust, yAdjust, size);
				this.graphics.endFill();
			
		}
		
		private function getColor(size:Number):uint
		{
			trace (size);
			var returnColor:uint = 0xFFFFFF;
			
			if (size < 1)
			{
				returnColor = 0x8888AA;
			}
			
			else if (size <= blockWidth / 3 && size > 1)
			{
				returnColor = 0xAAAAAA;
			}
			
			else if (size >= blockWidth-50 && size <= blockWidth)
			{
				returnColor = 0x888888;
			}
			
			return returnColor;
			
		}
		
		public function checkIfBlank():Boolean
		{
			if (distanceFromWhite > color)
			{
				return true;
			}
			
			else 
			{
				return false;
			}
		}
		
	}

}