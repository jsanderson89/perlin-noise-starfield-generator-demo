package  
{
	import BaseClasses.Utilities.Keycodes;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	/**
	 * ...
	 * @author a
	 */
	public class PerlinGenerator extends Sprite
	{
		private var myBitmapDataObject:BitmapData;
		private var bitmap:BitmapProcessorController;
		private var myBitmap:Bitmap;
		private var gameStage:Stage;
		
		public var blockSize:int = 100; 
		
		public var cellArray:Array;
		public var cellHolder:Sprite;
		
		public function PerlinGenerator(mainStage:Stage) 
		{
			trace("HOMO");
			var i:Number = 0xFFFFFF - 9079434;
			var j:Number = 0x000000 - 9079434;
			trace(i);
			trace(j);
			
			gameStage = mainStage;
			gameStage.focus = gameStage;
			gameStage.addEventListener(KeyboardEvent.KEY_UP, handleKeyboardEvent);
			this.addEventListener(MouseEvent.CLICK, handleMouseClick);
		}
		
		public function handleKeyboardEvent(e:KeyboardEvent):void
		{
			trace("pressed");
			/*
			if (e.keyCode == Keycodes.SPACE_KEY)
			{
				if (bitmap != null)
				{
					removeChild(bitmap);
					bitmap = null;
					cellArray = null;
					cellHolder = null;
				}
				
				if (myBitmapDataObject == null && myBitmap == null)
				{
					createBitmap();
				}
				
				else if (myBitmap != null || myBitmapDataObject != null)
				{
					if (myBitmapDataObject != null)
					{
						myBitmapDataObject = null;
					}
				
					if (myBitmap != null)
					{
						removeChild(myBitmap);
						myBitmap = null;
					}
					
				}
			}*/
			/*
			else if (e.keyCode == Keycodes.ENTER_KEY)
			{
				trace("pressed");
				if (cellArray != null)
				{
					cellHolder = new Sprite();
					
					for (var i:int = 0; i <= (cellArray.length - 1); i++)
					{
						cellHolder.cellArray[i].draw();
						cellHolder.addChild(cellArray[i]);
						
					}
				}
			}*/
		}
		
		private function createBitmap():void
		{
			myBitmapDataObject = new BitmapData(800, 600);
			var seed:Number = Math.floor(Math.random()*100);
			myBitmapDataObject.perlinNoise(200, 200, 1, seed, true, true, 7, true, null);
			myBitmap = new Bitmap(myBitmapDataObject);
			addChild(myBitmap);
		}
		
		private function handleMouseClick(e:MouseEvent):void
		{
			if (bitmap != null)
			{
				if (bitmap.proccessComplete == true)
				{
					removeChild(bitmap);
					bitmap = null;
					cellArray = null;
					cellHolder = null;
				}
			}
			
			else
			{
				bitmap = new BitmapProcessorController(myBitmapDataObject, blockSize);
				addChild(bitmap);
				cellArray = bitmap.cellArray;
			}
		}
		
	}
}


/*
public function scanArray(scannerSize:int, bitmap:BitmapData):void
		{
			var totalPixels:int = (bitmap.width * bitmap.height);
			trace(totalPixels);
			var array:Array = new Array();
			
			for (var s:int = 1; s <= scannerSize; s++)
			{
				var arrayLine:Array = new Array();
				for (var arrayLineCell:int = 0; arrayLineCell <= scannerSize; arrayLineCell++)
				{
					var pixel:Pixel = new Pixel();
					arrayLine.push(pixel);
				}
					
				array.push(arrayLine);
			}
			trace(array);
			trace(array.length);
			
			
			var scanner:Rectangle = new Rectangle(0, 0, scannerSize, scannerSize);
			trace(bitmap.getPixels(scanner));
			
			for (var x:int = 0; x < bitmap.width; x++)
			{
				for (var y:int = 0; y < bitmap.height; y++)
				{
					var target:Number = bitmap.getPixel(x, y);
					var red:Number = 0;
					red += target >> 16 & 0xFF;
					var green:Number = 0;
					green += target >> 8 & 0xFF;
					var blue:Number = 0;
					blue += target & 0xFF;
				}
				
			}
		}
		
		public static function averageColours( source:BitmapData, colours:int ):Array
		{
			var averages:Array = new Array();
			var columns:int = Math.round( Math.sqrt( colours ) );

			var row:int = 0;
			var col:int = 0;

			var x:int = 0;
			var y:int = 0;

			var w:int = Math.round( source.width / columns );
			var h:int = Math.round( source.height / columns );

			for (var i:int = 0; i < colours; i++)
			{
				var rect:Rectangle = new Rectangle( x, y, w, h );

				var box:BitmapData = new BitmapData( w, h, false );
				box.copyPixels( source, rect, new Point() );

				averages.push( averageColour( box ) );
				box.dispose();

				col = i % columns;

				x = w * col;
				y = h * row;

				if ( col == columns - 1 ) row++;
			}

		return averages;
		}
*/