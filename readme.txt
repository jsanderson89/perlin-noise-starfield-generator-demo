Perlin Noise Starfield Generator


An AS3 test which uses a perlin noise bitmap averaged into tiles by hue
to distribute shapes into a "starfield."

Controls:
	-Space = Generate perlin noise bitmap
	-Enter = Average the pixels of perlin noise bitmap into tiles
	-Backspace = Distribute shapes based on tile hue
	-Up arrow = Display/Hide bitmap
	-Down arrow = Display/Hide tiles


Copyright Jesse Anderson, 2013,2014.